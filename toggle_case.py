import sublime, sublime_plugin

class ToggleCaseCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        sels = self.view.sel()
        for sel in sels:
            if sel.a == sel.b :
                sel = self.view.word(sel)
                
            sel_string = self.view.substr(sel)

            first = sel_string[0]
            if(first == first.upper()) :
                sel_string = sel_string.lower()
            else :
                sel_string = sel_string.upper()
            self.view.replace(edit, sel, sel_string)
